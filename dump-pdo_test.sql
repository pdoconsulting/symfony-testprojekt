-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Czas generowania: 01 Kwi 2016, 10:51
-- Wersja serwera: 10.1.10-MariaDB
-- Wersja PHP: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `pdo_test`
--
CREATE DATABASE IF NOT EXISTS `pdo_test` DEFAULT CHARACTER SET utf8 COLLATE utf8_german2_ci;
USE `pdo_test`;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `werkstattdaten`
--

DROP TABLE IF EXISTS `werkstattdaten`;
CREATE TABLE `werkstattdaten` (
  `id` int(11) NOT NULL,
  `name_besitzer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vorname_besitzer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `strasse` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hausnummer` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `adresszusatz` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plz` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `ort` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `land` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `werkstattdaten`
--

INSERT INTO `werkstattdaten` (`id`, `name_besitzer`, `vorname_besitzer`, `email`, `strasse`, `hausnummer`, `adresszusatz`, `plz`, `ort`, `land`) VALUES
(1, 'Musterman', 'Max', 'test@pdo-consulting.de/', 'Strasse', '123', NULL, 'D – 80939', 'München', 'Deutschland'),
(2, 'Mustermann', 'Erika', 'emuster@pdo-consulting.de', 'Musterstrasse', '321', 'abc', 'A-12345', 'Stadt', 'Deutschland'),
(3, 'Name', 'Vorname', 'test123@pdo-consulting.de/', 'Hauptstraße', '91d', NULL, 'B-123456', 'Musterdorf', 'Deutschland'),
(4, 'Doe', 'John', 'test332@pdo-consulting.de/', 'Qwerty', '456', NULL, 'XY-987', 'Abcdef', 'USA'),
(5, 'Kowalski', 'Jan', 'test4@pdo-consulting.de/', 'Piękna', '15', NULL, '03-968', 'Warszau', 'Polen'),
(6, 'Public', 'Jane', 'test354@pdo-consulting.de/', 'Street', '232', NULL, 'AB-4677', 'London', 'UK'),
(11, 'Nowak', 'Jan', 'test@pdo-consulting.de/', 'Strasse', '354', 'bfgbgf', '00-000', 'Ort', 'Polen'),
(12, 'Kowalska', 'Janina', 'test@pdo-consulting.de/', 'Piękna', '97', 'abc', '01-234', 'Pozen', 'Polen'),
(13, 'Mustermann', 'Max', 'test@pdo-consulting.de/', 'Strasse', '46', 'qwerty', 'Y-09876', 'Berlin', 'Deutschland'),
(14, 'Kowalski', 'Jan', 'test@pdo-consulting.de/', 'Strasse', '35', NULL, 'D-346457', 'Stadt', 'Deutschland'),
(15, 'Kowalski', 'Jan', 'test@pdo-consulting.de/', 'Strasse', '35', NULL, 'D-346457', 'Stadt', 'Deutschland'),
(16, 'Mustermann', 'Max', 'test@pdo-consulting.de/', 'Strasse', '465', NULL, 'C-88796', 'Ort', 'Land'),
(19, 'Kowalski', 'Janek', 'test@pdo-consulting.de/', 'Ulica', '12', NULL, '00-000', 'Miasteczko', 'Polska'),
(28, 'test', 'setrbtnuym', 'imyunb', 'ebtrytnuym', 'yunt', 'brntumy', 'myuntb', 'iuym', 'ebrntm'),
(29, 'Musterman', 'Max', 'test@pdo-consulting.de/', 'Strasse', '123', NULL, 'D – 80939', 'München', 'Deutschland'),
(30, 'Musterman', 'Max', 'test@pdo-consulting.de/', 'Strasse', '123', NULL, 'D – 80939', 'München', 'Deutschland');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `werkstattdaten`
--
ALTER TABLE `werkstattdaten`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `werkstattdaten`
--
ALTER TABLE `werkstattdaten`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
