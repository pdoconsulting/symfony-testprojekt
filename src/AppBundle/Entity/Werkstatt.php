<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="werkstattDaten")
 *
 */
class Werkstatt
{
	/**
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;
	
	/**
	 * @ORM\Column(type="string", length=255)
	 */
	protected $nameBesitzer;
	
	/**
	 * @ORM\Column(type="string", length=255)
	 */
	protected $vornameBesitzer;
	
	/**
	 * @ORM\Column(type="string", length=255)
	 */
	protected $email;
	
	/**
	 * @ORM\Column(type="string", length=255)
	 */
	protected $strasse;
	
	/**
	 * @ORM\Column(type="string", length=25)
	 */
	protected $hausnummer;
	
	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	protected $adresszusatz;
	
	/**
	 * @ORM\Column(type="string", length=10)
	 */
	protected $plz;
	
	/**
	 * @ORM\Column(type="string", length=255)
	 */
	protected $ort;
	
	/**
	 * @ORM\Column(type="string", length=255)
	 */
	protected $land;
	
	

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nameBesitzer
     *
     * @param string $nameBesitzer
     *
     * @return Werkstatt
     */
    public function setNameBesitzer($nameBesitzer)
    {
        $this->nameBesitzer = $nameBesitzer;

        return $this;
    }

    /**
     * Get nameBesitzer
     *
     * @return string
     */
    public function getNameBesitzer()
    {
        return $this->nameBesitzer;
    }

    /**
     * Set vornameBesitzer
     *
     * @param string $vornameBesitzer
     *
     * @return Werkstatt
     */
    public function setVornameBesitzer($vornameBesitzer)
    {
        $this->vornameBesitzer = $vornameBesitzer;

        return $this;
    }

    /**
     * Get vornameBesitzer
     *
     * @return string
     */
    public function getVornameBesitzer()
    {
        return $this->vornameBesitzer;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Werkstatt
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set strasse
     *
     * @param string $strasse
     *
     * @return Werkstatt
     */
    public function setStrasse($strasse)
    {
        $this->strasse = $strasse;

        return $this;
    }

    /**
     * Get strasse
     *
     * @return string
     */
    public function getStrasse()
    {
        return $this->strasse;
    }

    /**
     * Set hausnummer
     *
     * @param string $hausnummer
     *
     * @return Werkstatt
     */
    public function setHausnummer($hausnummer)
    {
        $this->hausnummer = $hausnummer;

        return $this;
    }

    /**
     * Get hausnummer
     *
     * @return string
     */
    public function getHausnummer()
    {
        return $this->hausnummer;
    }

    /**
     * Set adresszusatz
     *
     * @param string $adresszusatz
     *
     * @return Werkstatt
     */
    public function setAdresszusatz($adresszusatz)
    {
        $this->adresszusatz = $adresszusatz;

        return $this;
    }

    /**
     * Get adresszusatz
     *
     * @return string
     */
    public function getAdresszusatz()
    {
        return $this->adresszusatz;
    }

    /**
     * Set plz
     *
     * @param string $plz
     *
     * @return Werkstatt
     */
    public function setPlz($plz)
    {
        $this->plz = $plz;

        return $this;
    }

    /**
     * Get plz
     *
     * @return string
     */
    public function getPlz()
    {
        return $this->plz;
    }

    /**
     * Set ort
     *
     * @param string $ort
     *
     * @return Werkstatt
     */
    public function setOrt($ort)
    {
        $this->ort = $ort;

        return $this;
    }

    /**
     * Get ort
     *
     * @return string
     */
    public function getOrt()
    {
        return $this->ort;
    }

    /**
     * Set land
     *
     * @param string $land
     *
     * @return Werkstatt
     */
    public function setLand($land)
    {
        $this->land = $land;

        return $this;
    }

    /**
     * Get land
     *
     * @return string
     */
    public function getLand()
    {
        return $this->land;
    }
}
