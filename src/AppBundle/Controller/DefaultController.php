<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\Length;
use AppBundle\Entity\Werkstatt;

class DefaultController extends Controller
{	
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
        ]);
    }
    
    /**
     * Check if saved record is unique
     *
     * @Route("/werkstattcheck", name="werkstatt_check")
     */
    public function ajaxcheckAction(Request $request)
    {
    	if (!$request->isXmlHttpRequest()) {
    		return new JsonResponse(array('message' => 'Sie können das nur mit Ajax zugreifen!', 'status' => false), 400);
    	}
    	
    	$requestFormParams = $request->request->get('werkstatt');
    	
    	$em = $this->getDoctrine()->getManager();
    	$werkstatts = $em->getRepository('AppBundle:Werkstatt')->findBy(
    			array(
    				'nameBesitzer' => $requestFormParams['nameBesitzer'], 
    				'vornameBesitzer' => $requestFormParams['vornameBesitzer'],
    				'strasse' => $requestFormParams['strasse'],
    				'hausnummer' => $requestFormParams['hausnummer'],
    				'plz' => $requestFormParams['plz']
    			)
    	);
    	
    	// echo print_r($werkstatts);
    	 
    	
    	return new JsonResponse(array(
    			'message' => ( ($werkstatts) ? 'moeglicher Duplikat' : ''),
    			'duplikat'  => ( ($werkstatts) ? true : false), 
    			'status' => true
    	));
    }
    
    /**
     * helper method to check if imported data is valid
     * 
     * @param unknown $row
     * @return boolean
     */
    protected function validateImportedRow($row) {
    	If (count($row) !=9) return false;
    	// only adresszusatz is not required (index 5)
    	for ($i=0; $i<9; $i++) {
    		if ($i != 5) {
    			if (empty($row[$i])) return false;
    			// validate e-mail 
    			if (2 == $i) {
    				if (filter_var($row[2], FILTER_VALIDATE_EMAIL) === false) return false;
    			} 
    		}
    	}
    	return true;
    }
    
    /**
     * Import data from CSV 
     * 
     * @Route("/werkstattimport", name="werkstatt_import")
     * 
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function importAction(Request $request) {
    	$form = $this->createFormBuilder()
    	->add('submitFile', FileType::class, array('label' => 'Datei zu importieren'))
    	->add('delimiter', TextType::class, array('label' => 'Trennzeichen', 'empty_data' => ',', 'required' => true, 'constraints' => new Length(array('min' => 1, 'max' => 1))))
    	->getForm();
    	
    	$i = 0;    	
    	$em = $this->getDoctrine()->getManager();
    	
    	if ($request->getMethod('post') == 'POST') {
    		$form->handleRequest($request);
    	
    		if ($form->isValid()) {
    			$file = $form->get('submitFile');
    			$delimiter = $form->get('delimiter')->getData();
    	
    			$filename = $file->getData();
    			$row = 1;
    			if (($handle = fopen($filename, "r")) !== FALSE) {
    				$batchSize = 20;
    				while (($data = fgetcsv($handle, 100, $delimiter)) !== FALSE) {
    					$row++;
    					
    					if ($this->validateImportedRow($data)) {
	    					$werkstatt = new Werkstatt();
	    					$werkstatt->setNameBesitzer($data[0]);
	    					$werkstatt->setVornameBesitzer($data[1]);
	    					$werkstatt->setEmail($data[2]);
	    					$werkstatt->setStrasse($data[3]);
	    					$werkstatt->setHausnummer($data[4]);
	    					$werkstatt->setAdresszusatz($data[5]);
	    					$werkstatt->setPlz($data[6]);
	    					$werkstatt->setOrt($data[7]);
	    					$werkstatt->setLand($data[8]);
	    					
	    					$em->persist($werkstatt);
	    					
	    					// print_r($em); die();
	    						    					
	    					if (($i % $batchSize) === 0) {
	    						$em->flush();
	    						$em->clear();
	    					}
	    					$i++; 
    					} 
    				}
    				
    				$em->flush();
    				$em->clear();
    				
    				fclose($handle);
    			}
    		} 
    	
    	} 
    	
    	if ($request->getMethod('post') == 'POST') {
    		$this->get('session')->getFlashBag()->add(
    				'notice',
    				'Es wurde ' . $i . ' Datenzeilen importiert.'
    		);
    	} 
    	
    	return $this->render('import.html.twig',
    			array(
    				'form' => $form->createView()
    			)
    	);
    }
    
}
