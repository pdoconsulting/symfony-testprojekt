<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Werkstatt;
use AppBundle\Form\WerkstattType;

/**
 * Werkstatt controller.
 *
 * @Route("/werkstatt")
 */
class WerkstattController extends Controller
{
    /**
     * Lists all Werkstatt entities.
     *
     * @Route("/", name="werkstatt_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $werkstatts = $em->getRepository('AppBundle:Werkstatt')->findAll();

        return $this->render('werkstatt/index.html.twig', array(
            'werkstatts' => $werkstatts,
        ));
    }

    /**
     * Creates a new Werkstatt entity.
     *
     * @Route("/new", name="werkstatt_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $werkstatt = new Werkstatt();
        $form = $this->createForm('AppBundle\Form\WerkstattType', $werkstatt);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($werkstatt);
            $em->flush();

            return $this->redirectToRoute('werkstatt_show', array('id' => $werkstatt->getId()));
        }

        return $this->render('werkstatt/new.html.twig', array(
            'werkstatt' => $werkstatt,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Werkstatt entity.
     *
     * @Route("/{id}", name="werkstatt_show")
     * @Method("GET")
     */
    public function showAction(Werkstatt $werkstatt)
    {
        $deleteForm = $this->createDeleteForm($werkstatt);

        return $this->render('werkstatt/show.html.twig', array(
            'werkstatt' => $werkstatt,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Werkstatt entity.
     *
     * @Route("/{id}/edit", name="werkstatt_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Werkstatt $werkstatt)
    {
        $deleteForm = $this->createDeleteForm($werkstatt);
        $editForm = $this->createForm('AppBundle\Form\WerkstattType', $werkstatt);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($werkstatt);
            $em->flush();

            return $this->redirectToRoute('werkstatt_edit', array('id' => $werkstatt->getId()));
        }

        return $this->render('werkstatt/edit.html.twig', array(
            'werkstatt' => $werkstatt,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    
    

    /**
     * Deletes a Werkstatt entity.
     *
     * @Route("/{id}", name="werkstatt_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Werkstatt $werkstatt)
    {
        $form = $this->createDeleteForm($werkstatt);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($werkstatt);
            $em->flush();
        }

        return $this->redirectToRoute('werkstatt_index');
    }

    /**
     * Creates a form to delete a Werkstatt entity.
     *
     * @param Werkstatt $werkstatt The Werkstatt entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Werkstatt $werkstatt)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('werkstatt_delete', array('id' => $werkstatt->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
